# Верстка: HTML, CSS #1

> Домашнее задание

## Задание

В качестве домашнего задания к занятию необходимо закончить верстку HTML-страницы по [макету](https://www.figma.com/file/8DiMW3hQytSNDG0xDlVnhe/Shopcart---Online-Ecommerce-website-(Community)?node-id=0%3A1&t=VSFweUH4bHjSrQZ2-1), включая стилизацию при помощи CSS.

Презентация с занятия размещена [здесь](https://docs.google.com/presentation/d/1NRaPuuA-bJ3h7YlBzqP9M9ZtiEZkidKUfFpeUvSEaAQ/edit?usp=sharing).

## Как сдавать

1. Сделать **fork** репозитория в личный репозиторий.

![](docs/fork.png)

При редактировании деталей не забудьте проверить, что выбрано пространство соответствующее вашему аккаунту, а также что видимость репозитория &mdash; публичная.

![](docs/fork-details.png)

2. Дать доступ к репозиторию преподавателям

Для этого во вкладке **Project information** зайти в раздел **Members**

![](docs/members.jpg)


![](docs/invite-members.png)

Далее нажать **Invite members** и в поле **Username or email address** ввести @usual-one и @Ilya_Senchenko. В качестве role выбрать Developer.

![](docs/member-usernames.jpg)

Нажать **Invite**.

3. Загрузить код **НЕ в main-ветку**.

Выполните следующее:
```sh
git clone <repo-url>
git checkout -b <branch-name>
```

Скопируйте ваш код в склонированный репозиторий. Далее:
```sh
git commit -m "HW-1"
git push -u origin <branch-name>
```

где 
* &lt;repo-url&gt; &mdash; URL **вашего** репозитория
![](docs/clone.png)
* &lt;branch-name&gt; &mdash; Название ветки (может быть любым).

4. Сделайте **merge request** и назначьте его на одного из преподавателей

![](docs/merge-start.png)

Укажите ветку, созданную на 2-м шаге. 
![](docs/merge-branch.png)

**Важно!** И в source, и в target должен быть выбран ваш репозиторий!

В качестве Reviewer укажите @usual-one или @Ilya_Senchenko.
![](docs/merge-reviewer.png)
![](docs/merge-finish.png)

4. Ожидайте проверки :)